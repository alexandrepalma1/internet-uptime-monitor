package gummy.uptime_monitor.view;

import java.awt.Color;
import java.awt.HeadlessException;

import javax.swing.JFrame;

import se.datadosen.component.RiverLayout;

public class MainFrame extends JFrame {
	private static final long serialVersionUID = -1744620432442906752L;
	public static final MainFrame INSTANCE = new MainFrame();
	
	public MainFrame() throws HeadlessException {
		setSize(800, 600);
		setLayout(new RiverLayout());
		initComponents();
		setupComponents();
		addComponents();
		getContentPane().setBackground(Color.black);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private void addComponents() {
		add("center", new StatusTimerLabel());
		setResizable(false);
	}

	private void setupComponents() {
		
	}

	private void initComponents() {
		
	}
}
