package gummy.uptime_monitor.view;

import java.awt.Color;
import java.awt.Font;

import gummy.uptime_monitor.UptimeMonitor;
import gummy.uptime_monitor.Util;

import javax.swing.JLabel;

public class StatusTimerLabel extends JLabel {

	private static final long serialVersionUID = 1L;

	public StatusTimerLabel() {
		setFont(new Font("Courier New", Font.BOLD, 40));
		new Thread(new Runnable() {

			public void run() {
				while(true) {
					if(UptimeMonitor.INSTANCE.isOnline() == true) {
						setForeground(Color.GREEN);
					} else {
						setForeground(Color.RED);
					}
					setText(Util.getHoraMinutoSegundoAsString(System.currentTimeMillis() - UptimeMonitor.INSTANCE.getLastStateChangeTimestamp()));
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
					}
					repaint();
					updateUI();
					revalidate();
				}
			}
		}).start();
	}
}
