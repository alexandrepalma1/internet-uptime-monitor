package gummy.uptime_monitor.types;

import java.util.Date;

import com.assembla.gummy.database.info.DataBaseEntity;
import com.assembla.gummy.database.info.DataBaseField;
@DataBaseEntity(tableName="CONNECTION_STATUS_CHANGE")
public class TOConnectionStatusChange {
	@DataBaseField(columnName="ID")
	private Integer id;
	@DataBaseField(columnName="CREATED_AT")
	private Date createdAt;
	@DataBaseField(columnName="ONLINE")
	private Boolean online;
	public Integer getId() {
		return id;
	}
	public TOConnectionStatusChange(Boolean online) {
		this.online = online;
	}
	public TOConnectionStatusChange() {
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public void setOnline(Boolean online) {
		this.online = online;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public Boolean getOnline() {
		return online;
	}
}
