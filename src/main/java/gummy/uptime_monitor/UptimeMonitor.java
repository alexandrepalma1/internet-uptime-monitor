package gummy.uptime_monitor;

import static ez.abstract_logger.StaticLoggerWrapper.*;
import gummy.uptime_monitor.db.DB;
import gummy.uptime_monitor.impl.EchoConnectionChecker;
import gummy.uptime_monitor.spec.ConnectionChecker;
import gummy.uptime_monitor.types.TOConnectionStatusChange;

public class UptimeMonitor {
	
	public static final UptimeMonitor INSTANCE = new UptimeMonitor();
	
	private Boolean online, running;
	private long lastStateChangeTimestamp, totalUptime, totalDownTime;
	
	private ConnectionChecker connectionChecker;
	
	private UptimeMonitor() {
		lastStateChangeTimestamp = System.currentTimeMillis();
		connectionChecker = new EchoConnectionChecker();
	}

	public long getTotalUptime() {
		return totalUptime;
	}

	public long getTotalDownTime() {
		return totalDownTime;
	}

	public void start() {
		running = true;
		new Thread(new Runnable() {
			
			public void run() {
				while(running) {
					boolean onlineStatus = connectionChecker.isOnline();
					if(online == null || onlineStatus != online) {
						long newTimestamp = System.currentTimeMillis();
						long timeInPreviousStatus = newTimestamp - lastStateChangeTimestamp;
						if(onlineStatus) {
							totalDownTime += timeInPreviousStatus;
						} else {
							totalUptime += timeInPreviousStatus;
						}
						lastStateChangeTimestamp = newTimestamp;
						online = onlineStatus;
						logChange(timeInPreviousStatus);
						DB.INSTANCE.insertConnectionStatusChange(new TOConnectionStatusChange(online));
					}
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
	
	public long currentStatusTime() {
		return System.currentTimeMillis() - lastStateChangeTimestamp;
	}
	
	public Boolean isOnline() {
		return online != null && online;
	}

	public long getLastStateChangeTimestamp() {
		return lastStateChangeTimestamp;
	}

	public void stop() {
		running = false;
	}
	
	private void logChange(long timeInPreviousStatus) {
		logInfo("Status: "+(online ? "online" : "offline")+"\t| Status changed after: " +  Util.getHoraMinutoSegundoAsString(timeInPreviousStatus) + "\t| " + totalUptimeAndDowntime());
	}
	
	private String totalUptimeAndDowntime() {
		return "Total Uptime: " + Util.getHoraMinutoSegundoAsString(totalUptime) +" \t| Total Downtime: " +  Util.getHoraMinutoSegundoAsString(totalDownTime);
	}
	
	
}
