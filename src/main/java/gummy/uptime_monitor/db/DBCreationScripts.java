package gummy.uptime_monitor.db;

import java.sql.SQLException;
import java.sql.Statement;

public class DBCreationScripts implements gummy.database.derby.DerbyDBCreator.ScriptRunner {

	public void runScripts(Statement statement) throws SQLException {
		statement.execute("CREATE TABLE CONNECTION_STATUS_CHANGE ("
				+ "ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),"
				+ "ONLINE BOOLEAN,"
				+ "CREATED_AT TIMESTAMP  DEFAULT CURRENT_TIMESTAMP,"
				+ "CONSTRAINT PK_TRY PRIMARY KEY (ID))");
	}
}
