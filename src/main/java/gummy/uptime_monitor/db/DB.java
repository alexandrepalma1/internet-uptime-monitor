package gummy.uptime_monitor.db;

import java.util.Date;

import gummy.database.GummyClient;
import gummy.database.GummyPooledConnectionSettings;
import gummy.database.derby.DerbyConnectionSettings;
import gummy.uptime_monitor.types.TOConnectionStatusChange;


public class DB {
	
	private GummyPooledConnectionSettings connectionSettings;
	public static final DB INSTANCE = new DB();
	
	
	public void insertConnectionStatusChange(TOConnectionStatusChange object) {
		getGummyClient().executeInsertTO(object);
	}
	
	public TOConnectionStatusChange[] selectTOConnectionStatusChange(Date dtfrom, Date dtTo) {
		return getGummyClient().executeQueryTO("SELECT * FROM CONNECTION_STATUS_CHANGE WHERE CREATED_AT >= ? AND CREATED_AT <= ?", TOConnectionStatusChange.class, new Object[]{dtfrom, dtTo});
	}
	
	private DB() {
		connectionSettings = new DerbyConnectionSettings("uptime_monitor", "UM");
	}
	
	private GummyClient getGummyClient() {
		return GummyClient.getInstance(connectionSettings);
	}
}
