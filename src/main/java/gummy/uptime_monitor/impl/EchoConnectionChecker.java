package gummy.uptime_monitor.impl;

import gummy.uptime_monitor.spec.ConnectionChecker;

import com.assembla.gummy.http_client.HttpClientWrapper;

public class EchoConnectionChecker implements ConnectionChecker{

	private HttpClientWrapper client;

	public EchoConnectionChecker() {
		super();
		client = new HttpClientWrapper(500);
		
		 
	}

	public boolean isOnline() {
		try {
			String timeMilis = String.valueOf(System.currentTimeMillis());
			String response = client.getContent("http://echo-test.herokuapp.com/?param=" + timeMilis, 1000);
			return timeMilis.equals(response);
		} catch(Exception e) {

		}
		return false;
	}
}