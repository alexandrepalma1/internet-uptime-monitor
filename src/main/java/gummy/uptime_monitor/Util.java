package gummy.uptime_monitor;

public class Util {
	public static String getHoraMinutoSegundoAsString(long milis) {
		long segundos = milis / 1000;
		long h = segundos / 60 / 60;
		long minutos = (segundos / 60) % 60;
		long minSegundos = segundos % 60;

		return concatenaZero(h) + ":" + concatenaZero(minutos) + ":" + concatenaZero(minSegundos);
	}

	private static String concatenaZero(long i) {
		return String.valueOf((i < 10 ? "0"+i : i));
	}
}
