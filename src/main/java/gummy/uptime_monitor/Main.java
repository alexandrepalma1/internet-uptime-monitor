package gummy.uptime_monitor;

import gummy.database.derby.DerbyDBCreator;
import gummy.uptime_monitor.db.DB;
import gummy.uptime_monitor.db.DBCreationScripts;
import gummy.uptime_monitor.types.TOConnectionStatusChange;
import gummy.uptime_monitor.view.MainFrame;
import gummy.util.DateUtil;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.derby.impl.sql.catalog.SYSPERMSRowFactory;
import org.apache.log4j.lf5.util.DateFormatManager;

public class Main {

	public static void main(String[] args) throws IOException, ParseException {
		DerbyDBCreator.createDB("uptime_monitor", new DBCreationScripts());
		UptimeMonitor.INSTANCE.start();
//		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//		 TOConnectionStatusChange[] t = DB.INSTANCE.selectTOConnectionStatusChange(dateFormat.parse("2001-01-01"), dateFormat.parse("2020-01-01"));
		 MainFrame.INSTANCE.setVisible(true);
	}
}
