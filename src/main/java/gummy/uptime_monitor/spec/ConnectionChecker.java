package gummy.uptime_monitor.spec;

public interface ConnectionChecker {
	boolean isOnline();
}
